
from django.db import models
from django.db.models.signals import pre_save
from django.utils.text import slugify
from django.shortcuts import reverse
from django.db.models import Q
from django.contrib.auth.models import User
from django.core.validators import MaxValueValidator, MinValueValidator
from django.utils.safestring import mark_safe

class Color(models.Model):
    name = models.CharField(max_length=50)

    def __str__(self):
        return self.name


class Size(models.Model):
    name = models.CharField(max_length=50)

    def __str__(self):
        return self.name

class Category(models.Model):
    title = models.CharField(max_length=150)
    banner = models.ImageField(upload_to='category_images')
    slug = models.SlugField(unique=True, blank=True, null=True)
    
    def __str__(self):
        return self.title
    
    def get_absolute_url(self):
        return reverse("catalog:category-detail", kwargs={'slug': self.slug})


class Brand(models.Model):
    title = models.CharField(max_length=150)
    banner = models.ImageField(upload_to='brand_images')
    slug = models.SlugField(unique=True, blank=True, null=True)

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse("catalog:brand-detail", kwargs={'slug': self.slug})


class ProductManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset()


    def all(self):
        return self.get_queryset().filter(active=True)

    def featured(self):
        return self.get_queryset().filter(featured=True, active=True)

    def new(self):
        return self.get_queryset().filter(new=True, active=True)

    def brand(self, request, slug):
        slug = slug
        return Product.objects.all().filter(Q(brand__slug='Police'))

class Product(models.Model):
    title = models.CharField(max_length=150)
    description = models.TextField()
    sku = models.CharField(max_length=20, unique=True)
    slug = models.SlugField(unique=True)
    stock = models.IntegerField(default=10)
    image = models.ImageField(upload_to='product_images')
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    active = models.BooleanField(default=True)
    color = models.ForeignKey(Color, on_delete=models.CASCADE, blank=True)
    size = models.ForeignKey(Size, on_delete=models.CASCADE, blank=True)
    price = models.IntegerField(default=0)
    category = models.ManyToManyField(Category, related_name='product_categories', verbose_name=("Category"))
    brand = models.ForeignKey(Brand, verbose_name=("Brand"), related_name='product_brand', on_delete=models.CASCADE)
    featured = models.BooleanField(default=False)
    new = models.BooleanField(default=False)
    showing_individually = models.BooleanField(default=True)

    def __str__(self):
        return self.title
    
    objects = ProductManager()

    def get_absolute_url(self):
        return reverse("catalog:product-detail", kwargs={'slug': self.slug})
    
    def image_tag(self):
        if self.image.url is not None:
            return mark_safe('<img src="{}" height="50"/>'.format(self.image.url))
        else:
            return ""
    
    def get_price(self):
        return "{:.2f}".format(self.price / 100)

    @property
    def in_stock(self):
        return self.stock > 0

def pre_save_product_receiver(sender, instance, *args, **kwargs):
    if not instance.slug:
        instance.slug = slugify(instance.title)
pre_save.connect(pre_save_product_receiver, sender=Product)



class ProductImages(models.Model):
    product = models.ForeignKey(Product, related_name='product_image', on_delete=models.CASCADE)
    image = models.ImageField(blank=True, upload_to='product_images/')
    title = models.CharField(max_length=50,blank=True)

    def __str__(self):
        return self.product.title


    class Meta:
        verbose_name = 'Product Image'





class ProductReview(models.Model):
    product = models.ForeignKey(Product, related_name='product_review', on_delete=models.CASCADE)
    customer = models.ForeignKey(User, related_name='review_owner', on_delete=models.CASCADE)
    rating = models.PositiveIntegerField(default=0 ,  validators=[MaxValueValidator(5)])
    feedback = models.TextField(default='' , max_length=200)


    def __str__(self):
        return self.product.title



# class Cart(models.Model):
#     user        = models.ForeignKey(User, null=True, blank=True)
#     products    = models.ManyToManyField(Product, blank=True)
#     subtotal    = models.DecimalField(default=0.00, max_digits=100, decimal_places=2)
#     total       = models.DecimalField(default=0.00, max_digits=100, decimal_places=2)
#     updated     = models.DateTimeField(auto_now=True)
#     timestamp   = models.DateTimeField(auto_now_add=True)

#     def __str__(self):
#         return str(self.id)



# JD Cart Order and Payment

# class Address(models.Model):
#     ADDRESS_CHOICES = (
#         ('B', 'Billing'),
#         ('S', 'Shipping'),
#     )

#     user = models.ForeignKey(User, on_delete=models.CASCADE)
#     address_line_1 = models.CharField(max_length=150)
#     address_line_2 = models.CharField(max_length=150)
#     city = models.CharField(max_length=100)
#     zip_code = models.CharField(max_length=20)
#     address_type = models.CharField(max_length=1, choices=ADDRESS_CHOICES)
#     default = models.BooleanField(default=False)

#     def __str__(self):
#         return f"{self.address_line_1}, {self.address_line_2}, {self.city}, {self.zip_code}"

#     class Meta:
#         verbose_name_plural = 'Addresses'


# class Cart(models.Model):
#     order = models.ForeignKey(
#         "Order", related_name='items', on_delete=models.CASCADE)
#     product = models.ForeignKey(Product, on_delete=models.CASCADE)
#     quantity = models.PositiveIntegerField(default=1)
#     colour = models.ForeignKey(ColourVariation, on_delete=models.CASCADE)
#     size = models.ForeignKey(SizeVariation, on_delete=models.CASCADE)
    
#     def __str__(self):
#         return f"{self.quantity} x {self.product.title}"

#     def get_raw_total_item_price(self):
#         return self.quantity * self.product.price

#     def get_total_item_price(self):
#         price = self.get_raw_total_item_price()  # 1000
#         return "{:.2f}".format(price / 100)


# class Order(models.Model):
#     user = models.ForeignKey(User, blank=True, null=True, on_delete=models.CASCADE)
#     start_date = models.DateTimeField(auto_now_add=True)
#     ordered_date = models.DateTimeField(blank=True, null=True)
#     ordered = models.BooleanField(default=False)

#     billing_address = models.ForeignKey(
#         Address, related_name='billing_address', blank=True, null=True, on_delete=models.SET_NULL)
#     shipping_address = models.ForeignKey(
#         Address, related_name='shipping_address', blank=True, null=True, on_delete=models.SET_NULL)

#     def __str__(self):
#         return self.reference_number

#     @property
#     def reference_number(self):
#         return f"ORDER-{self.pk}"

#     def get_raw_subtotal(self):
#         total = 0
#         for order_item in self.items.all():
#             total += order_item.get_raw_total_item_price()
#         return total

#     def get_subtotal(self):
#         subtotal = self.get_raw_subtotal()
#         return "{:.2f}".format(subtotal / 100)

#     def get_raw_total(self):
#         subtotal = self.get_raw_subtotal()
#         # add tax, add delivery, subtract discounts
#         # total = subtotal - discounts + tax + delivery
#         return subtotal

#     def get_total(self):
#         total = self.get_raw_total()
#         return "{:.2f}".format(total / 100)
        

# class Payment(models.Model):
#     order = models.ForeignKey(
#         Order, on_delete=models.CASCADE, related_name='payments')
#     payment_method = models.CharField(max_length=20, 
#     choices=(
#         ('PayPal', 'PayPal'), 
#         ('Cash On Delivery', 'Cash On Delivery')
#     )
#     )
#     timestamp = models.DateTimeField(auto_now_add=True)
#     successful = models.BooleanField(default=False)
#     amount = models.FloatField()
#     raw_response = models.TextField()

#     def __str__(self):
#         return self.reference_number

#     @property
#     def reference_number(self):
#         return f"PAYMENT-{self.order}-{self.pk}"


