from django.shortcuts import get_object_or_404, reverse, redirect
from django.views import generic
from .models import Product, Brand, Category
from django.contrib import messages
from django.db.models import Q



class all(generic.ListView):
    template_name = 'catalog/product_list.html'
    queryset = Product.objects.all()



class featured(generic.ListView):
    template_name = 'catalog/product_list.html'
    queryset = Product.objects.featured()

class new(generic.ListView):
    template_name = 'catalog/product_list.html'
    queryset = Product.objects.new()

class ProductDetailView(generic.DetailView):
    model = Product
    template_name='catalog/product_detail.html'



class all_brands(generic.ListView):
    template_name = 'catalog/brands_list.html'
    queryset = Brand.objects.all()


class BrandProductsList(generic.ListView):
    model = Product
    template_name='catalog/brand_detail.html'

    def get_queryset(self):
        qs = self.model.objects.all()
        if self.kwargs.get('slug'):
            qs = qs.filter(brand__slug=self.kwargs['slug'])
        return qs


class all_categories(generic.ListView):
    template_name = 'catalog/category_list.html'
    queryset = Category.objects.all()

    
class CategoryProductsList(generic.ListView):
    model = Product
    template_name='catalog/category_detail.html'

    def get_queryset(self):
        qs = self.model.objects.all()
        if self.kwargs.get('slug'):
            qs = qs.filter(category__slug=self.kwargs['slug'])
        return qs
