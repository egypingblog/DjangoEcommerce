from django.urls import path
from .views import HomeView, ContactView


app_name='homepage'

urlpatterns = [
    path('', HomeView.as_view(), name='home'),#homepage:home
    path('contact/', ContactView.as_view(), name='contact'),
    #path('profile',views.profile , name='profile'),
    #path('profile/edit',views.profile_edit , name='profile_edit'),
]