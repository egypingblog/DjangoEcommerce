from django.urls import path
from . import views

app_name = 'catalog'

urlpatterns = [
    #path('', views.CartView.as_view(), name='summary'),
    # Products all - featured - new 
    path('', views.all.as_view(), name='product-list'),
    path('featured', views.featured.as_view(), name='featured-list'),
    path('new', views.new.as_view(), name='new-list'),
    path('shop/<slug>/', views.ProductDetailView.as_view(), name='product-detail'),
    #Brand
    path('brands', views.all_brands.as_view(), name='brand-list'),
    path('brand/<slug>/', views.BrandProductsList.as_view(), name='brand-detail'),
    #Category
    path('category/<slug>/', views.CategoryProductsList.as_view(), name='category-detail'),
    path('categories', views.all_categories.as_view(), name='category-list'),
]



# JD Cart payment and order
    # path('increase-quantity/<pk>/',views.IncreaseQuantityView.as_view(), name='increase-quantity'),
    # path('decrease-quantity/<pk>/',views.DecreaseQuantityView.as_view(), name='decrease-quantity'),
    # path('remove-from-cart/<pk>/',views.RemoveFromCartView.as_view(), name='remove-from-cart'),
    # path('checkout/', views.CheckoutView.as_view(), name='checkout'),
    # path('payment/', views.PaymentView.as_view(), name='payment'),
    # path('thank-you/', views.ThankYouView.as_view(), name='thank-you'),
    # path('confirm-order/', views.ConfirmOrderView.as_view(), name='confirm-order'),
    # path('orders/<pk>/', views.OrderDetailView.as_view(), name='order-detail'),