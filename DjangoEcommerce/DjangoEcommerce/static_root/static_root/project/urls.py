from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static



urlpatterns = [
    path('', include('homepage.urls', namespace='homepage')), #homepage:home , homepage:contact
    path('admin/', admin.site.urls),
    path('accounts/', include('allauth.urls')),
    path('catalog/', include('catalog.urls', namespace='catalog')), # catalog:product-list , 
]

if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL,
                          document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL,
                          document_root=settings.MEDIA_ROOT)


admin.site.site_header = 'EgyPing Store'
admin.site.site_title = 'EgyPing Store'


    # JD Profile view
    #path('profile/', ProfileView.as_view(), name='profile'),
    # JD Staff App
    #path('staff/', include('staff.urls', namespace='staff')),