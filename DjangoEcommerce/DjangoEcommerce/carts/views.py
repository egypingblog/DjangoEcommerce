from django.conf import settings
from django.http import JsonResponse
from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponse

from accounts.forms import LoginForm, GuestForm
from accounts.models import GuestEmail

from addresses.forms import AddressCheckoutForm
from addresses.models import Address

from billing.models import BillingProfile
from orders.models import Order
from catalog.models import Product
from .models import Cart, CartItem
from django.contrib import messages
import requests
import json


# MyFatoorah 
token = "rLtt6JWvbUHDDhsZnfpAhpYk4dxYDQkbcPTyGaKp2TYqQgG7FGZ5Th_WD53Oq8Ebz6A53njUoo1w3pjU1D4vs_ZMqFiz_j0urb_BH9Oq9VZoKFoJEDAbRZepGcQanImyYrry7Kt6MnMdgfG5jn4HngWoRdKduNNyP4kzcp3mRv7x00ahkm9LAK7ZRieg7k1PDAnBIOG3EyVSJ5kK4WLMvYr7sCwHbHcu4A5WwelxYK0GMJy37bNAarSJDFQsJ2ZvJjvMDmfWwDVFEVe_5tOomfVNt6bOg9mexbGjMrnHBnKnZR1vQbBtQieDlQepzTZMuQrSuKn-t5XZM7V6fCW7oP-uXGX-sMOajeX65JOf6XVpk29DP6ro8WTAflCDANC193yof8-f5_EYY-3hXhJj7RBXmizDpneEQDSaSz5sFk0sV5qPcARJ9zGG73vuGFyenjPPmtDtXtpx35A-BVcOSBYVIWe9kndG3nclfefjKEuZ3m4jL9Gg1h2JBvmXSMYiZtp9MR5I6pvbvylU_PP5xJFSjVTIz7IQSjcVGO41npnwIxRXNRxFOdIUHn0tjQ-7LwvEcTXyPsHXcMD8WtgBh-wxR8aKX7WPSsT1O8d8reb2aR7K3rkV3K82K_0OgawImEpwSvp9MNKynEAJQS6ZHe_J_l77652xwPNxMRTMASk1ZsJL"
baseURL = "https://apitest.myfatoorah.com"
url = baseURL + "/v2/ExecutePayment"
mf_check_status_url = "https://apitest.myfatoorah.com/v2/GetPaymentStatus"


def cart_detail_api_view(request):
    cart_obj, new_obj = Cart.objects.new_or_get(request)
    products = [{
            "id": x.id,
            "url": x.get_absolute_url(),
            "name": x.name, 
            "price": x.price
            } 
            for x in cart_obj.products.all()]
    cart_data  = {"products": products, "subtotal": cart_obj.subtotal, "total": cart_obj.total}
    return JsonResponse(cart_data)

def cart_home(request):
    cart_obj, new_obj = Cart.objects.new_or_get(request)
    cart_items = CartItem.objects.filter(cart=cart_obj)

    subtotal = 0
    for cart_item in cart_items:
        subtotal += (cart_item.product.price * cart_item.quantity)
        cart_obj.subtotal = subtotal
        cart_obj.save()
    return render(request, "carts/home.html", dict(
        items = cart_items,
        cart = cart_obj,
        subtotal = subtotal, 
        # counter = counter, 
        # data_key = data_key, 
        # stripe_total = stripe_total, 
        # description = description))
    ))


# cart_update function to handle Add to Cart button
def Add_To_Cart(request):
    product_id = request.POST.get('product_id')
    product_obj = Product.objects.get(id=product_id)
    cart_obj, new_obj = Cart.objects.new_or_get(request)

    try:
        cart_item = CartItem.objects.get(product=product_obj, cart=cart_obj)
        if cart_item.quantity < cart_item.product.stock:
            cart_item.quantity += 1
        cart_item.save()
    except CartItem.DoesNotExist:
        CartItem.objects.create(product = product_obj,quantity = 1,cart = cart_obj)
    

    return redirect("cart:home")


def Add_Product_Cart_Quantity(request, pk):
    cart_obj, new_obj = Cart.objects.new_or_get(request)
    product = get_object_or_404(Product, id=pk)
    cart_item = CartItem.objects.get(product=product, cart=cart_obj)
    if cart_item.quantity < cart_item.product.stock:
        cart_item.quantity += 1
        cart_item.save()
    else:
        return ("No enough quantity")
    return redirect('cart:home')

def Reduce_Product_Cart_Quantity(request, pk):
    cart_obj, new_obj = Cart.objects.new_or_get(request)
    product = get_object_or_404(Product, id=pk)
    cart_item = CartItem.objects.get(product=product, cart=cart_obj)
    try:
        if cart_item.quantity != 1:
            cart_item.quantity -= 1
            cart_item.save()
        else:
            cart_item.delete() 
    		   
    except:
        pass
    try:
        if cart_item.quantity == 1:
           pass
    except:
         pass
    return redirect('cart:home')


def Remove_From_Cart(request, pk):
    cart_obj, new_obj = Cart.objects.new_or_get(request)
    product = get_object_or_404(Product, id=pk)
    cart_item = CartItem.objects.get(product=product, cart=cart_obj)
    cart_item.delete()
    return redirect('cart:home')

# Checkout without login prompt, shipping and billing addresses and profiles
def checkout_home(request):
    cart_obj, cart_created = Cart.objects.new_or_get(request)
    cart_items = CartItem.objects.filter(cart=cart_obj)
    order_obj = None
    login_form = LoginForm(request=request)
    guest_form = GuestForm(request=request)
    address_form = AddressCheckoutForm()
    billing_address_id = request.session.get("billing_address_id", None)
    shipping_address_id = request.session.get("shipping_address_id", None)

    billing_profile, billing_profile_created = BillingProfile.objects.new_or_get(request)
    address_qs = None

    if billing_profile is not None:
        if request.user.is_authenticated:
            address_qs = Address.objects.filter(billing_profile=billing_profile)        
        order_obj, order_obj_created = Order.objects.new_or_get(billing_profile, cart_obj)

        if shipping_address_id:
            order_obj.shipping_address = Address.objects.get(id=shipping_address_id)
            del request.session["shipping_address_id"]

        if billing_address_id:
            order_obj.billing_address = Address.objects.get(id=billing_address_id) 
            del request.session["billing_address_id"]

        if billing_address_id or shipping_address_id:
            order_obj.save()
    
    

    if request.method == "POST":
        CallBackUrl = f"http://127.0.0.1:8000/cart/checkout/mfsuccess/"
        ErrorUrl = f"http://127.0.0.1:8000/cart/checkout/mferror/"
        MF_KNET_ID = 1
        MF_CC_ID = 2
        order_id = order_obj.order_id
        payload = json.dumps(
            {
        "PaymentMethodId":MF_CC_ID,
        "CustomerName": MF_CC_ID,
        "DisplayCurrencyIso": "KWD",
        "InvoiceValue": 100,
        "CallBackUrl": CallBackUrl,
        "ErrorUrl": ErrorUrl,
        "Language": "en",
        "CustomerReference" :order_id,
        }
        )

        headers = {'Content-Type': "application/json", 'Authorization': "Bearer " + token}
        r = requests.post(url, data=payload, headers=headers)
        valid_request = r.status_code in range(200, 299)

        if valid_request:
            ResponseData = r.json()
            URL = ResponseData['Data']["PaymentURL"]

            for item in cart_items:
                products = Product.objects.get(id=item.product.id)
                products.stock = int(item.product.stock - item.quantity)
                products.save()
                #item.delete()

            return redirect(URL)


    context = {
        "object": order_obj,
        "billing_profile": billing_profile,
        "login_form": login_form,
        "guest_form": guest_form,
        "address_form": address_form,
        "address_qs": address_qs,
    }

    return render(request, "carts/checkout.html", context)



def checkout_done_view(request):
    return render(request, "carts/checkout-done.html")



def MyFatoorah_Success(request):
    cart_obj, cart_created = Cart.objects.new_or_get(request)
    cart_items = CartItem.objects.filter(cart=cart_obj)

    payment_id = request.GET.get('paymentId')

    payload = json.dumps(
            {
        "Key": payment_id,
        "KeyType": "PaymentId"
    }
    )
    
    headers = {'Content-Type': "application/json", 'Authorization': "Bearer " + token}
    r = requests.post(mf_check_status_url, data=payload, headers=headers)

    response = r.json()
    success = response['IsSuccess']
    order_num = response['Data']["CustomerReference"]
    payment_date = response['Data']["CreatedDate"]


    order_obj, order_obj_created = Order.objects.get_by_cart(cart_obj)
    order_obj.mark_paid_online() # working

    order_obj.mark_payment_method_mf_cc()
    request.session['cart_items'] = 0  # Items counts 
    del request.session['cart_id'] # working

    order_obj.payment_id = payment_id
    order_obj.save()

    context = {
        "payment_id": payment_id,
        "success": success,
        "order_number": order_num,
        "payment_date": payment_date,
    }
    
    return render(request, "carts/checkout-mf-success.html", context)




def MyFatoorah_Error(request):
    cart_obj, cart_created = Cart.objects.new_or_get(request)
    cart_items = CartItem.objects.filter(cart=cart_obj)


    id_ = request.GET.get('id') # retrieving the `id`
    payment_id = request.GET.get('paymentId') # retrieving the `paymentId`


    payload = json.dumps(
            {
        "Key": payment_id,
        "KeyType": "PaymentId"
    }
    )
    
    headers = {'Content-Type': "application/json", 'Authorization': "Bearer " + token}
    r = requests.post(mf_check_status_url, data=payload, headers=headers)
    response = r.json()
    order_num = response['Data']["CustomerReference"]
    payment_date = response['Data']["CreatedDate"]


    order_obj, order_obj_created = Order.objects.get_by_cart(cart_obj)
    order_obj.mark_failed() # working

    order_obj.payment_id = payment_id
    order_obj.save()
    
    order_obj.mark_payment_method_mf_cc()

    for item in cart_items:
                products = Product.objects.get(id=item.product.id)
                products.stock = int(item.product.stock + item.quantity)
                products.save()
    
    context = {
        "payment_id": payment_id,
        "order_number": order_num,
        "payment_date": payment_date,

    }

    return render(request, "carts/checkout-mf-error.html", context)




