from django.urls import path

from .views import (
        cart_home, 
        Add_To_Cart, 
        checkout_home,
        checkout_done_view,
        Remove_From_Cart,
        Add_Product_Cart_Quantity,
        Reduce_Product_Cart_Quantity,
        MyFatoorah_Success,
        MyFatoorah_Error
        )

app_name = 'cart'

urlpatterns = [
    path('', cart_home, name='home'),
    path('update/', Add_To_Cart, name='update'),
    path('remove/<int:pk>/', Remove_From_Cart, name='remove'),
    path('add_quantity/<int:pk>/', Add_Product_Cart_Quantity, name='add_quantity'),
    path('reduce_quantity/<int:pk>/', Reduce_Product_Cart_Quantity, name='reduce_quantity'),
    path('checkout/success/', checkout_done_view, name='success'),
    path('checkout/mfsuccess/', MyFatoorah_Success, name='mf_success'),
    path('checkout/mferror/', MyFatoorah_Error, name='mf_error'),
    path('checkout/', checkout_home, name='checkout'),
]