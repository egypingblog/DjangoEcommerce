from django.contrib import admin


from .models import Cart, CartItem


class CartItemAdmin(admin.ModelAdmin):
      list_display =[
    'id',
    '__str__',
    'CartID',
    'product',
    'quantity',
    'sub_total',
  ]


admin.site.register(Cart)
admin.site.register(CartItem, CartItemAdmin)