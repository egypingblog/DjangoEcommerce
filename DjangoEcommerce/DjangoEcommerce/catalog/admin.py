from django.contrib import admin
from .models import Product, Category, Brand, ProductImages, Color, Size #Cart, Order,   Address, Payment, 
import admin_thumbnails


@admin_thumbnails.thumbnail('image')
class ProductImagesInline(admin.TabularInline):
    model = ProductImages
    readonly_fields = ('id',)
    extra = 1


@admin_thumbnails.thumbnail('image')
class ProductImagesAdmin(admin.ModelAdmin):
    list_display = ['image','title','image_thumbnail']



class BrandAdmin(admin.ModelAdmin):
      list_display =[
    'id',
    'title',
    'slug',
  ]

class CategoryAdmin(admin.ModelAdmin):
      list_display =[
    'id',
    'title',
    'slug',
  ]


class ProductAdmin(admin.ModelAdmin):
  list_display =[
    'title',
    'sku',
    'active',
    'price',
    'stock',
    'featured',
    'new',
    'brand',
  ]
  list_filter = ['category']
  readonly_fields = ('image_tag',)
  inlines = [ProductImagesInline]
  prepopulated_fields = {'slug': ('title',)}


admin.site.register(Product, ProductAdmin)
admin.site.register(Size)
admin.site.register(Color)
admin.site.register(Category, CategoryAdmin)
admin.site.register(Brand, BrandAdmin)
admin.site.register(ProductImages,ProductImagesAdmin)


# JD Cart Order Payment 
# class AddressAdmin(admin.ModelAdmin):
#     list_display = [
#         'address_line_1',
#         'address_line_2',
#         'city',
#         'zip_code',
#         'address_type',
#     ]

# class CartAdmin(admin.ModelAdmin):
#     list_display = [
#         'pk',
#         'order',
#         'product',
#         'quantity',
#         'colour',
#         'size',
#     ]

# class OrderAdmin(admin.ModelAdmin):
#     list_display = [
#         'pk',
#         'reference_number',
#         'user',
#         'ordered',
#         'start_date',
#         'ordered_date',
#     ]

# class PaymentAdmin(admin.ModelAdmin):
#     list_display = [
#         'pk',
#         'reference_number',
#         #'user',
#         'order',
#         'payment_method',
#         'successful',
#         'amount',
#     ]

# admin.site.register(Address, AddressAdmin)
# admin.site.register(Cart, CartAdmin)
# admin.site.register(Order, OrderAdmin)
# admin.site.register(Payment, PaymentAdmin)

