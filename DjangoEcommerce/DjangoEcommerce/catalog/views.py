from django.shortcuts import get_object_or_404, reverse, redirect
from django.views import generic
from .models import Product, Brand, Category
from django.contrib import messages
from django.db.models import Q

from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import ListView, DetailView, View
from carts.models import Cart, CartItem

class UserProductHistoryView(LoginRequiredMixin, ListView):
    template_name = "products/user-history.html"
    def get_context_data(self, *args, **kwargs):
        context = super(UserProductHistoryView, self).get_context_data(*args, **kwargs)
        cart_obj, new_obj = Cart.objects.new_or_get(self.request)
        context['cart'] = cart_obj
        return context

    def get_queryset(self, *args, **kwargs):
        request = self.request
        views = request.user.objectviewed_set.by_model(Product, model_queryset=False)
        return views

class all(generic.ListView):
    template_name = 'catalog/product_list.html'
    queryset = Product.objects.all()


class featured(generic.ListView):
    template_name = 'catalog/product_list.html'
    queryset = Product.objects.featured()

class new(generic.ListView):
    template_name = 'catalog/product_list.html'
    queryset = Product.objects.new()

class ProductDetailView(generic.DetailView):
    model = Product
    template_name='catalog/product_detail.html'
    
    def get_context_data(self, *args, **kwargs):
        context = super(ProductDetailView, self).get_context_data(*args, **kwargs)
        cart_obj, new_obj = Cart.objects.new_or_get(self.request)
        product = get_object_or_404(Product, slug=self.kwargs["slug"])
        cart_item = None
        
        try:
            cart_item = CartItem.objects.get(product=product, cart=cart_obj)
        except CartItem.DoesNotExist:
            pass

        context['cartitem'] = cart_item
        
        return context



class all_brands(generic.ListView):
    template_name = 'catalog/brands_list.html'
    queryset = Brand.objects.all()


class BrandProductsList(generic.ListView):
    model = Product
    template_name='catalog/brand_detail.html'

    def get_queryset(self):
        qs = self.model.objects.all()
        if self.kwargs.get('slug'):
            qs = qs.filter(brand__slug=self.kwargs['slug'])
        return qs


class all_categories(generic.ListView):
    template_name = 'catalog/category_list.html'
    queryset = Category.objects.all()

    
class CategoryProductsList(generic.ListView):
    model = Product
    template_name='catalog/category_detail.html'

    def get_queryset(self):
        qs = self.model.objects.all()
        if self.kwargs.get('slug'):
            qs = qs.filter(category__slug=self.kwargs['slug'])
        return qs
