from django.contrib import admin
from django.urls import path, include, re_path
from django.conf import settings
from django.conf.urls.static import static
from accounts.views import LoginView, RegisterView, GuestRegisterView

from carts.views import cart_detail_api_view

from django.views.generic import TemplateView, RedirectView
from accounts.views import LoginView, RegisterView, GuestRegisterView

from addresses.views import (
    AddressCreateView,
    AddressListView,
    AddressUpdateView,
    checkout_address_create_view, 
    checkout_address_reuse_view
    )



urlpatterns = [
    path('', include('homepage.urls', namespace='homepage')), #homepage:home , homepage:contact
    path('admin/', admin.site.urls),
    path('accounts/', include("accounts.urls", namespace='account')),
    #path('accounts/', include('allauth.urls')),
    path('catalog/', include('catalog.urls', namespace='catalog')), # catalog:product-list , 
    path('cart/', include("carts.urls", namespace='cart')),
    path('settings/', RedirectView.as_view(url='/account')),
    path('login/', LoginView.as_view(), name='login'),
    path('checkout/address/create/', checkout_address_create_view, name='checkout_address_create'),
    path('checkout/address/reuse/', checkout_address_reuse_view, name='checkout_address_reuse'),
    path('register/guest/', GuestRegisterView.as_view(), name='guest_register'),
    path('orders/', include("orders.urls", namespace='orders')),
    path('address/', RedirectView.as_view(url='/addresses')),
    path('addresses/', AddressListView.as_view(), name='addresses'),
    path('addresses/create/', AddressCreateView.as_view(), name='address-create'),
    re_path('addresses/(?P<pk>\d+)/', AddressUpdateView.as_view(), name='address-update'),
    path('accounts/', include("accounts.passwords.urls")),
]

if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL,
                          document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL,
                          document_root=settings.MEDIA_ROOT)


admin.site.site_header = 'EgyPing Store'
admin.site.site_title = 'EgyPing Store'


    # JD Profile view
    #path('profile/', ProfileView.as_view(), name='profile'),
    # JD Staff App
    #path('staff/', include('staff.urls', namespace='staff')),